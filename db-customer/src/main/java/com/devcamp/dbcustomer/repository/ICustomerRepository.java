package com.devcamp.dbcustomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.devcamp.dbcustomer.model.Customer;

@Service
public interface ICustomerRepository extends JpaRepository<Customer, Long> {

}
