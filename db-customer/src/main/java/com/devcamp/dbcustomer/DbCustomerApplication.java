package com.devcamp.dbcustomer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbCustomerApplication.class, args);
	}

}
