package com.devcamp.dbcustomer.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.dbcustomer.model.Customer;
import com.devcamp.dbcustomer.repository.ICustomerRepository;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CustomerControler {
  @Autowired
  ICustomerRepository pCustomerRepository;

  @GetMapping("/customers")
  public ResponseEntity<List<Customer>> getAllCustomers() {
    try {
      List<Customer> listCustomer = new ArrayList<Customer>();

      pCustomerRepository.findAll()
          .forEach(listCustomer::add);

      return new ResponseEntity<>(listCustomer, HttpStatus.OK);

    } catch (Exception ex) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
